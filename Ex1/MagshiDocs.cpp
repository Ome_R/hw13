#include "MagshiDocs.h"
#include <fstream>
#include <thread>
#include <iostream>

#define FILE_PATH "shared_doc.txt"

using std::ofstream;
using std::fstream;
typedef std::pair<SOCKET, string> Pair;

MagshiDocs::MagshiDocs()
{
	fstream file(FILE_PATH);
	file >> fileContent;
	file.close();
}

/*Add an user to the user queue.*/
void MagshiDocs::addUser(SOCKET socket, string userName)
{
	users.push_back(Pair(socket, userName));
}

/*Remove an user from the user queue.*/
void MagshiDocs::removeUser(SOCKET socket)
{
	for (auto iter = users.begin(); iter != users.end(); iter++) {
		if (iter->first == socket) {
			users.erase(iter);
			break;
		}
	}
}

/*Get the first user in the queue.*/
string MagshiDocs::getCurrentUser()
{
	return users[0].second;
}

/*Get the second user in the queue.
If the size of the queue is one, it will return the first user.*/
string MagshiDocs::getNextUser()
{
	return users.size() == 1 ? users[0].second : users[1].second;
}

void printMap(MagshiDocs* docs) {
	auto users = docs->getAllUsers();
	for (auto iter = users.begin(); iter != users.end(); iter++) {
		std::cout << "Socket " << iter->first << ", Username " << iter->second << std::endl;
	}
}

/*Move all the users 1 step further.
The first user goes to the back.*/
void MagshiDocs::rollUsers()
{
	auto iter = users.front();
	users.pop_front();
	users.push_back(iter);
}

/*Returns the position of an user.
If the user isn't in the queue, it returns -1.*/
int MagshiDocs::getUserPosition(SOCKET socket)
{
	int counter = 0;
	for (auto iter = users.begin(); iter != users.end(); iter++) {
		counter++;
		if (iter->first == socket)
			return counter;
	}

	return -1;
}

deque<Pair> MagshiDocs::getAllUsers()
{
	return users;
}

/*Returns the file content.*/
string MagshiDocs::getFileContent()
{
	return fileContent;
}

/*Set the file content.*/
void MagshiDocs::setFileContent(string fileContent)
{
	this->fileContent = fileContent;
	//Save content to file with another thread.
	std::thread(&MagshiDocs::saveToFile, this).detach();
}

/*Save the file content to file.*/
void MagshiDocs::saveToFile()
{
	ofstream file(FILE_PATH);
	file << fileContent;
	file.close();
}
