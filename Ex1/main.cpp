#pragma comment(lib, "ws2_32.lib")

#include "Server.h"
#include "WSAInitializer.h"

#define SERVER_PORT 8826

int main() {
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(SERVER_PORT);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}

	return 0;
}