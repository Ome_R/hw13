#pragma once

#include <iostream>
#include <queue>
#include <mutex>

template<class E>
class SafeQueue {
private:
	std::queue<E> queue;
	std::mutex mtx;
public:
	/*Erase the first element and return it, thread safe*/
	E pop() {
		mtx.lock();
		E element = queue.front();
		queue.pop();
		mtx.unlock();
		return element;
	}

	/*Push an element to the back of the queue, thread safe*/
	void push(E element) {
		mtx.lock();
		queue.push(element);
		mtx.unlock();
	}

};
