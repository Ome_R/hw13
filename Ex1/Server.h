#pragma comment(lib, "ws2_32.lib")
#pragma once

#include "MagshiDocs.h"
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <string>

using std::string;

class Server{
public:
	Server();
	~Server();
	void serve(int port);

private:
	void accept();
	void clientHandler(SOCKET clientSocket);
	void updateAllClients();

	SOCKET _serverSocket;
	MagshiDocs docs;

};

