#pragma once

#include "SafeQueue.h"
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <deque>

using std::string;
using std::deque;
typedef std::pair<SOCKET, string> Pair;

class MagshiDocs {
public:
	/* Constructor */
	MagshiDocs();

	/* Methods for user queue */
	void addUser(SOCKET socket, string userName);
	void removeUser(SOCKET socket);
	string getCurrentUser();
	string getNextUser();
	void rollUsers();
	int getUserPosition(SOCKET user);
	deque<Pair> getAllUsers();

	/* Methods for file content */
	string getFileContent();
	void setFileContent(string fileContent);
private:
	deque<Pair> users;
	string fileContent;
	void saveToFile();
};