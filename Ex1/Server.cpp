#pragma comment(lib, "ws2_32.lib")

#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <exception>

Server::Server() {
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception("Error");
}

Server::~Server(){
	try{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}catch (...) {}
}

void Server::serve(int port){
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true){
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept(){
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread(&Server::clientHandler, this, client_socket).detach();
}

/*Handles the client management*/
void Server::clientHandler(SOCKET clientSocket)
{
	bool receiveMessage = true;

	//Should run until we get a finish message.
	while (receiveMessage){
		//Catching all exceptions here so the client won't get crashed.
		try {
			//Get the message code from the client.
			int messageCode = Helper::getMessageTypeCode(clientSocket);

			switch (messageCode)
			{
				case MT_CLIENT_LOG_IN: {
					//Receives the user name from the socket.
					std::string userName = Helper::getDataFromSocket(clientSocket, 2);
					//Adding the user to the user queue
					docs.addUser(clientSocket, userName);
					//Send the most updated data to the new user
					Helper::sendUpdateMessageToClient(clientSocket, 
						docs.getFileContent(), 
						docs.getCurrentUser(), 
						docs.getNextUser(), 
						docs.getUserPosition(clientSocket));
					break;
				}
				case MT_CLIENT_FINISH: {
					//Receives the file data from the user
					std::string fileData = Helper::getDataFromSocket(clientSocket, 5);
					//Set the file content to the one from the client
					docs.setFileContent(fileData);
					//Roll the queue
					docs.rollUsers();
					//Send an update to all the clients
					updateAllClients();
					break;
				}
				case MT_CLIENT_UPDATE: {
					//Receives the file data from the user
					std::string fileData = Helper::getDataFromSocket(clientSocket, 5);
					//Set the file content to the one from the client
					docs.setFileContent(fileData);
					//Send an update to all the clients
					updateAllClients();
					break;
				}
				case MT_CLIENT_EXIT: {
					//Remove the socket from the sockets map and the user from queue
					docs.removeUser(clientSocket);
					//Updating all users with most recent data
					updateAllClients();
					//We don't want to receive more messages from this user.
					receiveMessage = false;
					break;
				}
			}
		}
		catch (std::exception ex) {
			std::cout << "Error occured: " << ex.what() << std::endl;
		}
		catch (...) {

		}
	}

	//Closing the socket.
	closesocket(clientSocket);
}

/*Update all clients with the data.*/
void Server::updateAllClients()
{
	auto users = docs.getAllUsers();
	for (auto iter = users.begin(); iter != users.end(); iter++) {
		Helper::sendUpdateMessageToClient(
			iter->first,
			docs.getFileContent(),
			docs.getCurrentUser(),
			docs.getNextUser(),
			docs.getUserPosition(iter->first)
		);
	}
}

